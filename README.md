This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Gym App
Carter Wilson and Josh Wolsborn
This is an app created for a gym that I am a member of to help the owner and members keep track of their daily routines. It runs off of a very specific training plan, so the app has specific use cases as outlined below.

**Common use case 1**: Member of the gym wants to see their exercises so they know what weight, set, and reps to do for the daily routine. The steps are as follows:
User selects Member option from landing page
User inputs their name, the current week and block, and the gym they are a member of (currently not an important choice, but could be in the future) and clicks submit
User is presented with a list of their exercises

**Common use case 2**: A new member joins the gym and does test lifts. 
Owner selects owner from the landing page
Owner selects Add Client button
Owner inputs client name, gym, and phone number
For each test lift completed, owner selects the owner tab in the side menu, followed by the Add Max button, and inputs the clients’ name, the given exercise, and the weight


**Common use case 3**: The owner wants to add a new exercise
Owner selects “Owner” option from landing page
Owner inputs exercise name and description and clicks submit



## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
