import React from 'react';
import { Radio, Button, Card } from 'antd';
import "../styling/WorkoutCard.css"

//Component to format exercise data
class WorkoutCard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            exercise: props.exercise
        }
    }

    render() {
        return(
            <div>
                <div className="workout-card-wrapper">
                    <Card title={this.state.exercise.exercise_id} bordered={false} style={{ width: "auto" }}>
                        <p>{this.state.exercise.weight}</p>
                    </Card>
                </div>
            </div>
        )
    }
}

export default WorkoutCard