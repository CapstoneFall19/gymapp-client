import React from 'react';
import Select from 'react-select';
import { SERVER_URL } from '../config';

import {
  Link,
} from "react-router-dom";
import pageVariants from "./variants";
import { motion } from 'framer-motion';
import { Radio, Button } from 'antd';
import '../styling/MemberPage.css';


//Component for user input data needed to get their exercises
class MemberInput extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            clientName: null,
            clientId: null,
            gymName: "Cully",
            week: 1,
            block: 1,
            url: props.url,
            gymList: [],
            clients: [],
            clientNames: [],
            clientIdRel: {}
        }
        this.getGymSelected = this.getGymSelected.bind(this)
        this.getWeekSelected = this.getWeekSelected.bind(this)
        this.getBlockSelected = this.getBlockSelected.bind(this)
        this.onClickSubmit = this.onClickSubmit.bind(this)

        console.log("url: " + this.props.url)
    }

    componentDidMount() {
        fetch(`${SERVER_URL}/api/gym`)
            .then(res => res.json())
            .then((result) => {
                this.setState({
                    isLoaded: true,
                    gymList: result
                })
            })

        fetch(`${SERVER_URL}/api/client`)
            .then(res => res.json())
            .then(result => {
                let clientNameList = []
                let clientIdRelList = {}
                for (var i=0; i < result.length; i++) {
                    const clientName = result[i].name
                    clientNameList.push(clientName)
                    clientIdRelList[clientName] = result[i].client_id
                }

                this.setState({
                    isLoaded: true,
                    clients: result,
                    clientNames: clientNameList,
                    clientIdRel: clientIdRelList
                })
            })
    }

    getGymSelected(event) {
        this.setState({
            gymName: event.target.value
        })
    }

    getWeekSelected(event) {
        this.setState({
            week: parseInt(event.target.value)
        })
    }

    getBlockSelected(event) {
        this.setState({
            block: parseInt(event.target.value)
        })
    }

    //handles a selection made in the name <Select /> component
    handleChange = selectedOption => {
        const clientName = selectedOption.value
        const clientId = this.state.clientIdRel[clientName]
        this.setState({
            clientName: clientName,
            clientId: clientId
        })
    }

    onClickSubmit = (e) => {
        if (this.state.clientId == null) {
            e.preventDefault();
            alert("Please enter your name!");
        }
    }

    render(){
        const gymNameChoices = this.state.gymList.map(gym =>
            <Radio.Button value={gym.name} key={gym.gym_id}>{gym.name}</Radio.Button>
        )

        let nameList = []
        for (let i = 0; i < this.state.clientNames.length; i++) {
            nameList.push({
                value: this.state.clientNames[i],
                label: this.state.clientNames[i]
            })
        }

        return(
            <motion.div className="workout-form-wrapper page"
                        initial="initial"
                        animate="in"
                        exit="out"
                        variants={pageVariants}
            >
                <form className="workout-form"> 
                    <div className="select-wrapper">
                        <Select
                            className="name-select"
                            options={nameList} 
                            onChange={this.handleChange}
                            placeholder="Enter name"
                        />
                    </div>

                    <label htmlFor="gym">Gym:</label>
                    <Radio.Group size="large" name="gym" defaultValue="Cully" buttonStyle="solid" onChange={this.getGymSelected}>
                        {gymNameChoices}
                    </Radio.Group>

                    <label htmlFor="week">Week:</label>
                    <Radio.Group defaultValue="1" size="large" name="week" buttonStyle="solid" onChange={this.getWeekSelected}>
                        <Radio.Button value="1">1</Radio.Button>
                        <Radio.Button value="2">2</Radio.Button>
                        <Radio.Button value="3">3</Radio.Button>
                        <Radio.Button value="4">4</Radio.Button>
                    </Radio.Group>

                    <label htmlFor="block">Block:</label>
                    <Radio.Group defaultValue="1" size="large" name="block" buttonStyle="solid" onChange={this.getBlockSelected}>
                        <Radio.Button value="1">1</Radio.Button>
                        <Radio.Button value="2">2</Radio.Button>
                        <Radio.Button value="3">3</Radio.Button>
                        <Radio.Button value="4">4</Radio.Button>
                    </Radio.Group>

                    <Link to={{
                        pathname: "/memberOutput",
                        workoutData: {
                            gym: this.state.gymName,
                            week: this.state.week,
                            block: this.state.block,
                            name: this.state.clientName,
                            clientId: this.state.clientId
                        }
                        }}
                        onClick={this.onClickSubmit}
                    >
                        <Button type="primary" block>
                            Submit
                        </Button>
                    </Link>
                </form>
            </motion.div>
        )
    }
}

export default MemberInput;