import React from 'react';
import { Button, Form, Input } from 'antd';
import Select from 'react-select';
import '../styling/AddMember.css'
import { SERVER_URL } from '../config';
import pageVariants from "./variants";
import {motion} from "framer-motion";

//component for adding a new gym member
class AddMember extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: null,
            lastName: null,
            phone: null,
            gyms: [],
            gymNames: [],
            gymSelected: null
        }
        this.handleSuccessfulSubmit = this.handleSuccessfulSubmit.bind(this)
        this.getGymSelected = this.getGymSelected.bind(this)
    }

    componentDidMount() {
        fetch(`${SERVER_URL}/api/gym`)
            .then(res => res.json())
            .then(result => {
                this.setState({
                    gyms: result,
                })
            })
    }

    //handle form submission
    handleSuccessfulSubmit = (values) => {
        this.setState(
            {
                firstName: values.firstname,
                lastName: values.lastname,
                phone: values.phone
            }
        )

        const personData = {
            name: this.state.firstName + " " + this.state.lastName,
            gymId: parseInt(this.state.gymSelected),
            phone: this.state.phone
        }

        fetch(`${SERVER_URL}/api/client`,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(personData)
            })
            .then(res => {
                if (res.status === 201) {
                    alert("Member added successfully!")
                } else {
                    return res.json()
                }
            })
            .then(result => {
                if (result) {
                    alert(result.message)
                }
            })
            .catch(error => {
                console.log("failedfailedfailed")
            })
    }

    getGymSelected(selectedItem) {
        let gymId = selectedItem.value;
        this.setState({
            gymSelected: gymId
        })
    }

    render() {

        const gymChoices = this.state.gyms.map((gym) => {
            return {
                value: gym.gym_id,
                label: gym.name
            }
        })

        return (
            <motion.div className="page-wrapper page"
                        initial="initial"
                        animate="in"
                        exit="out"
                        variants={pageVariants}
            >
                <div className="form-wrapper">
                    <Form
                        name="member-entry"
                        onFinish={this.handleSuccessfulSubmit}
                    >
                        <Form.Item name="gym" label="Gym" required="true">
                            <Select 
                                options={gymChoices} 
                                onChange={this.getGymSelected} 
                                className="basic-single"
                                placeholder="Select Gym Name"/>
                        </Form.Item>
                        <Form.Item 
                            name="firstname"
                            label="First Name"
                            rules = {[
                                {
                                    required: true,
                                    message: "Please enter your first name"
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item 
                            name="lastname"
                            label="Last Name"
                            rules = {[
                                {
                                    required: true,
                                    message: "Please enter your last name"
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="phone"
                            label="Phone #"
                            rules = {[
                                {
                                    required: true,
                                    message: "Please enter your phone number"
                                },
                            ]}
                        >
                            <Input type="tel" />
                        </Form.Item>

                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form>
                </div>
            </motion.div>
        )
    }
}

export default AddMember