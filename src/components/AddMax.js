import React from 'react';
import { Button, Form, Input } from 'antd';
import Select from 'react-select';
import '../styling/AddMax.css'
import { SERVER_URL } from '../config';
import {motion} from "framer-motion";
import pageVariants from "./variants";

//Component for adding a new max lift for a given client
class AddMax extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            existingMaxes: null,
            clients: [],
            clientChoices: [],
            exercises: [],
            clientId: null,
            exerciseId: null,
            exerciseName: null,
            weight: null,
            exerciseChoices: []
        }
        this.handleSelectExercise.bind(this);
        this.handleSelectName.bind(this);
        this.onSubmit.bind(this);
        this.reloadExistingMaxes(this);
    }

    componentDidMount(){
        fetch(`${SERVER_URL}/api/client`)
            .then(res => res.json())
            .then(result => {
                let clientChoices = [];
                for (let i = 0; i < result.length; i++) {
                    clientChoices.push({
                        label: result[i].name,
                        value: result[i].client_id
                    })
                }
                this.setState({
                    clients: result,
                    clientChoices: clientChoices
                })
            })

        fetch(`${SERVER_URL}/api/exercise`)
            .then(res => res.json())
            .then(result => {
                let exerciseChoices = []
                for (let i = 0; i < result.length; i++) {
                    exerciseChoices.push({
                        label: result[i].name,
                        value: result[i].exercise_id
                    })
                }
                this.setState({
                    exercises: result,
                    exerciseChoices: exerciseChoices
                })
            })
    }

    //Reload the maxes for a client from the database, used to decide whether to post or put
    reloadExistingMaxes = () => {
        if (this.state.clientId != null) {
            fetch(`${SERVER_URL}/api/maxLift/${this.state.clientId}`)
            .then(res => res.json())
            .then(response => {
                this.setState({
                    existingMaxes: response
                })
            })
            .catch(error => console.log(error))
        }
    }

    //handles a selection for the <Select /> component for the client name
    handleSelectName = (selectedItem) => {
        const id = selectedItem.value
        fetch(`${SERVER_URL}/api/maxLift/${id}`)
            .then(res => res.json())
            .then(result => {
                this.setState({
                    existingMaxes: result,
                    clientId: selectedItem.value
                })
            })
            .catch(error => {
                console.log(error)
                this.setState({
                    existingMaxes: null,
                    clientId: selectedItem.value
                })
            })
    }

    //handles a selection for the <Select /> component for the exercise name
    handleSelectExercise = (selectedItem) => {
        this.setState({
            exerciseId: selectedItem.value,
            exerciseName: selectedItem.label
        })
    }

    //handles form submission
    onSubmit = (values) => {
        const exerciseData = {
            clientId: this.state.clientId,
            exerciseId: this.state.exerciseId,
            weight: values.weight
        }
        let exists = false;

        //checks if a max already exists for the given exercise and client
        if (this.state.existingMaxes != null) {
            for (var i = 0; i < this.state.existingMaxes.length; i++) {
                if (this.state.existingMaxes[i].name === this.state.exerciseName) {
                    exists = true;
                }
            }
        }

        if (exists === false) {
            fetch(`${SERVER_URL}/api/maxLift`,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(exerciseData)
            })
                .then(res => {
                    if (res.status === 201) {
                        alert("Max added successfully!")
                    } else {
                        return res.json()
                    }
                })
                .then(result => {
                    if (result) {
                        alert(result.message)
                    }
                    this.reloadExistingMaxes();
                })
                .catch(error => {
                    console.log(error)
                })
        }
        else {
            fetch(`${SERVER_URL}/api/maxLift`,
            {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(exerciseData)
            })
                .then(res => {
                    if (res.status === 200) {
                        alert("Max updated successfully!")
                    } else {
                        return res.json()
                    }
                })
                .then(result => {
                    if (result) {
                        alert(result.message)
                    }
                    this.reloadExistingMaxes();
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }

    render() {
        return(
            <motion.div className="main-wrapper page"
                        initial="initial"
                        animate="in"
                        exit="out"
                        variants={pageVariants}>
                <div className="addMax-wrapper">
                    <Form
                        onFinish={this.onSubmit}
                    >
                        <div className="select-name-wrapper">
                            <Form.Item name="client" label="Your Name" required="true">
                                <Select
                                    onChange={this.handleSelectName}
                                    options={this.state.clientChoices}
                                >
                                </Select>
                            </Form.Item>
                        </div>
                        <div className="select-exercise-wrapper">
                            <Form.Item name="exercise" label="Exercise name" required="true">
                                <Select
                                    onChange={this.handleSelectExercise}
                                    options={this.state.exerciseChoices}
                                >
                                </Select>
                            </Form.Item>
                        </div>
                        <Form.Item name="weight" label="Weight" required="true">
                            <Input/>
                        </Form.Item>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form>
                </div>
            </motion.div>
        )
    }
}

export default AddMax;
