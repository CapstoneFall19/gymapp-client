import React from 'react';

import { Button, Form, Input } from 'antd';
import '../styling/AddExercise.css'
import { SERVER_URL } from '../config';
import pageVariants from "./variants";
import {motion} from "framer-motion";

//Component used to add a new exercise

class AddExercise extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            exerciseName: null,
            exerciseDescription: null
        }
    }

    //handles form submission
    handleSubmit = (values) =>  {
        this.setState({
            exerciseName: values.exerciseName,
            exerciseDescription: values.exerciseDescription
        })

        const exerciseData = {
            name: values.exerciseName,
            description: values.exerciseDescription
        }

        fetch(`${SERVER_URL}/api/exercise`,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(exerciseData)
            })
            .then(res => {
                if (res.status === 201) {
                    alert("Exercise added successfully!")
                } else {
                    return res.json()
                }
            })
            .then(result => {
                if (result) {
                    alert(result.message)
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        return(
            <motion.div
                 className="main-wrapper page"
                 initial="initial"
                 animate="in"
                 exit="out"
                 variants={pageVariants}
            >
                <div className="add-exercise-wrapper">
                    <Form
                        name="exercise-entry"
                        onFinish={this.handleSubmit}
                    >
                        <Form.Item
                            name="exerciseName"
                            label="Exercise Name"
                            rules = {[
                                {
                                    required: true,
                                    message: "Please enter the exercise name"
                                },
                            ]}>
                            <Input/>
                        </Form.Item>

                        <Form.Item
                            name="exerciseDescription"
                            label="Exercise Description"
                            rules = {[
                                {
                                    required: true,
                                    message: "Please enter the exercise description"
                                },
                            ]}>
                            <Input.TextArea/>
                        </Form.Item>

                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form>
                </div>
            </motion.div>

        )
    }
}

export default AddExercise;