import React from "react";
import {SERVER_URL} from "../config";
import pageVariants from "./variants";
import {motion} from "framer-motion";
import Form from "antd/es/form";
import Select from 'react-select';
import '../styling/RemoveMember.css'
import {Button} from "antd";

//Component used to remove a member
class RemoveMember extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            clientIdToDelete: null,
            clientList: []
        }
        this.handleChange = this.handleChange.bind(this)
        this.onSubmitForm = this.onSubmitForm.bind(this)
        this.loadNames = this.loadNames.bind(this)
    }

    componentDidMount() {
        this.loadNames()
    }

    //handle picking name from the <Select /> component
    handleChange = selectedOption => {
        const id = selectedOption.value
        this.setState({
            clientIdToDelete: id
        })
    }

    //Load existing clients
    loadNames = () => {
        fetch(`${SERVER_URL}/api/client`)
            .then(res => res.json())
            .then(result => {
                this.setState({
                    clientList: result,
                    clientIdToDelete: null
                })
            })
            .catch(error => console.log(error))
    }

    onSubmitForm = (e) => {
        if (this.state.clientIdToDelete === null) {
            alert("Please enter the name of a member to remove!")
            e.preventDefault()
        }
        else {
            fetch(`${SERVER_URL}/api/client/${this.state.clientIdToDelete}`,
                {
                    method: "DELETE",
                })
                .then(res => {
                    if (res.status === 200) {
                        alert("Member removed successfully!")
                        this.loadNames()
                    } else {
                        return res.json()
                    }
                })
                .then(result => {
                    if (result) {
                        alert(result.message)
                    }
                    window.location.reload(false)
                })
                .catch(error => {
                    console.log(error)
                    alert("Error removing Member")
                })
        }
    }

    render() {

        let clientChoices = []
        for (let i = 0; i < this.state.clientList.length; i++) {
            clientChoices.push({
                value: this.state.clientList[i].client_id,
                label: this.state.clientList[i].name
            })
        }

        return (
            <motion.div className="remove-client-wrapper"
                        initial="initial"
                        animate="in"
                        exit="out"
                        variants={pageVariants}
            >
                <Form className="remove-form" onFinish={this.onSubmitForm}>
                    <p>Which client do you want to remove?</p>
                    <Form.Item>
                        <Select
                            className="name-select"
                            options={clientChoices}
                            onChange={this.handleChange}
                            placeholder="Enter name"
                        />
                    </Form.Item>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form>
            </motion.div>
        )
    }
}

export default RemoveMember