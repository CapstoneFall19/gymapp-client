//Variants for framer motion elements

const pageVariants = {
    initial: {
        opacity: 0,
        y: 5
    },
    in: {
        opacity: 1,
    },
    out: {
        scale: 0
    },
};

export default pageVariants;