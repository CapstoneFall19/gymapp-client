import React from 'react'
import {
    Link,
  } from "react-router-dom"
import { motion } from 'framer-motion';
import { Button, Typography } from 'antd';
import '../styling/Landing.css';
import pageVariants from "./variants";

//Component for the the initial landing page
class Landing extends React.Component {

    render() {
        return(
            <motion.div className="nav-wrapper page"
                        initial="initial"
                        animate="in"
                        exit="out"
                        variants={pageVariants}
            >
                <div className="nav-card">
                    <Typography.Title level={2}>Welcome! Are you a member or the owner?</Typography.Title>
                    <div className="nav-buttons">
                        <Link to="/memberPage">
                            <Button type="primary">Member</Button>
                        </Link>
                        <Link to="/ownerPage">
                            <Button type="primary">Owner</Button>
                        </Link>
                    </div>
                </div>
            </motion.div>
        )
    }
}

export default Landing