import React from 'react';
import {Carousel} from 'primereact/carousel';
import 'primereact/resources/themes/nova-light/theme.css'
import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'
import pageVariants from "./variants";
import { SERVER_URL } from '../config';

import { Card } from 'antd';
import '../styling/MemberOutput.css'
import {motion} from "framer-motion";

//Component for outputting a client's exercises
class MemberOutput extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: props.location.workoutData.name,
            gym: props.location.workoutData.gym,
            week: props.location.workoutData.week,
            block: props.location.workoutData.block,
            clientId: props.location.workoutData.clientId,
            exerciseList: [],
            blockMultiplier: 1,
            reps: 1,
            contentLoaded: false
        }
        this.exerciseTemplate = this.exerciseTemplate.bind(this);
    }

    componentDidMount() {
        fetch(`${SERVER_URL}/api/maxLift/${this.state.clientId}`)
        .then(res => res.json())
        .then(result => {
            this.setState({
                exerciseList: result,
                contentLoaded: true
            })
        })
        .catch(error => {
            console.log(error);
            this.setState({
                contentLoaded: true
            })
        })

        //Set the block multiplier for determining weight percentage
        let blockMultiplier = null;
        switch(this.state.block) {
            case 1:
                blockMultiplier = .6
                break
            case 2: 
                blockMultiplier = .7
                break
            case 3:
                blockMultiplier = .8
                break
            case 4:
                blockMultiplier = .9
                break
            default:
                blockMultiplier = 1;
        }

        //Set the number of reps based on the week number
        let reps = null;
        switch(this.state.week) {
            case 1:
                reps = 10
                break
            case 2: 
                reps = 8
                break
            case 3: 
                reps = 6
                break
            case 4: 
                reps = 5
                break
            default:
                reps = 10;
        }

        this.setState({
            blockMultiplier: blockMultiplier,
            reps: reps
        })
    }

    exerciseTemplate(exercise) {
        return(
            <div className="exercise-wrapper">
                <Card title={exercise.name} bordered={false} style={{ width: 300 }}>
                    <p>{exercise.description}</p>
                    <p>{exercise.weight * this.state.blockMultiplier}</p>
                    <p>Sets: 4</p>
                    <p>Reps: {this.state.reps}</p>
                </Card>
            </div>
        )
    }
    
    render() {
        // const exercises = this.state.exerciseList.map(exercise => {
        //     return(
        //         <WorkoutCard exercise={exercise}/>
        //     )
        // })

        // const arrows = true
        let exerciseDisplay = (<p></p>);
        if (this.state.exerciseList.length === 0 && this.state.contentLoaded === true) {
            exerciseDisplay = (
                <Card title={"No Exercises"}>
                    <p>Hi! It looks like you don't have any exercises yet.</p>
                    <p>If you'd like add one, choose add max from the menu and add your max for an exercise.</p>
                </Card>
            )
        }
        else if (this.state.contentLoaded === true) {
            exerciseDisplay = (
                <Carousel
                    value={this.state.exerciseList}
                    itemTemplate={this.exerciseTemplate}
                    style={{width: "100%"}}
                >
                </Carousel>
            )
        }

        return(
            <motion.div className="carousel-wrapper page"
                        initial="initial"
                        animate="in"
                        exit="out"
                        variants={pageVariants}
            >
                {exerciseDisplay}
            </motion.div>
        )
    }
}

export default MemberOutput