import React from 'react';
import {
  Link,
} from "react-router-dom";
import { Button } from 'antd';
import '../styling/OwnerPage.css'
import {motion} from "framer-motion";
import pageVariants from "./variants";

//Component for the owner navigation page
class OwnerPage extends React.Component {
    render() {
        return(
            <motion.div className="nav-wrapper page"
                        initial="initial"
                        animate="in"
                        exit="out"
                        variants={pageVariants}>
                <div className="buttons-wrapper">
                    <Link to="/addMember" style={{padding: "10px"}}>
                        <Button type="primary">Add Member</Button>
                    </Link>
                    <Link to="/removeMember" style={{padding: "10px"}}>
                        <Button type="primary">Remove Member</Button>
                    </Link>
                    <Link to="/addExercise" style={{padding: "10px"}}>
                        <Button type="primary">Add Exercise</Button>
                    </Link>
                </div>
            </motion.div>
        )
    }
}

export default OwnerPage