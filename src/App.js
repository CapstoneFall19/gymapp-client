import React, {useState} from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from "react-router-dom";
import { AnimatePresence } from 'framer-motion';
import Landing from './components/Landing';
import MemberPage from './components/MemberPage'
import OwnerPage from './components/OwnerPage';
import AddMember from'./components/AddMember';
import RemoveMember from './components/RemoveMember'
import logo from './Assets/cullystrengthlogo.png'
import { HomeOutlined } from '@ant-design/icons'
import { Menu, Drawer } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import AddExercise from './components/AddExercise';
import AddMax from './components/AddMax';
import MemberOutput from "./components/MemberOutput";
import MemberInput from './components/MemberPage'
import MenuOutlined from "@ant-design/icons/lib/icons/MenuOutlined";


function App() {
  //test
    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    return (
        <Router>
          <div className="top-banner">
              <MenuOutlined
                  onClick={showDrawer}
                  style={{ fontSize: "25px", color: "white"}}
              />
              <img src={logo} className="logo" alt="Gym Logo"/>
          </div>
          <div className="body-layout-wrapper">
              <Drawer
                  title={null}
                  placement="left"
                  closable={false}
                  onClose={onClose}
                  visible={visible}
                  width={"fit-content"}
              >
                      <Menu
                          style={{width: 256, height: "100%"}}
                          mode="inline"
                          theme={"dark"}
                      >
                          <Menu.Item key="/" icon={<HomeOutlined/>}>
                              <Link to="/" onClick={onClose}>
                                  Home
                              </Link>
                          </Menu.Item>
                          <SubMenu
                              key="owner"
                              title = {
                                  <span>
                              <span>Owner</span>
                          </span>
                              }
                          >
                              <Menu.Item key="add-member">
                                  <Link to="/addMember" onClick={onClose}>
                                      Add Member
                                  </Link>
                              </Menu.Item>
                              <Menu.Item key="remove-member">
                                  <Link to="/removeMember" onClick={onClose}>
                                      Remove Member
                                  </Link>
                              </Menu.Item>
                              <Menu.Item key="add-exercise">
                                  <Link to="/addExercise" onClick={onClose}>
                                      Add exercise
                                  </Link>
                              </Menu.Item>
                          </SubMenu>
                          <SubMenu
                              key="member"
                              title = {
                                  <span>
                              <span>Member</span>
                          </span>
                              }
                          >
                              <Menu.Item key="get-workouts">
                                  <Link to="/memberPage" onClick={onClose}>
                                      Daily Workout
                                  </Link>
                              </Menu.Item>
                              <Menu.Item key="add-max">
                                  <Link to="/addMax" onClick={onClose}>
                                      Add Max
                                  </Link>
                              </Menu.Item>
                          </SubMenu>
                      </Menu>
              </Drawer>

            <Route render={({location}) => (
                <AnimatePresence exitBeforeEnter>
                    <Switch location={location} key={location.pathname}>
                      <Route exact path="/">
                        <Landing/>
                      </Route>
                      <Route exact path="/memberPage">
                        <MemberPage/>
                      </Route>
                      <Route path="/ownerPage">
                        <OwnerPage/>
                      </Route>
                      <Route path="/addMember">
                        <AddMember/>
                      </Route>
                      <Route path={"/removeMember"}>
                          <RemoveMember />
                      </Route>
                      <Route path="/addExercise">
                        <AddExercise/>
                      </Route>
                      <Route path="/addMax">
                        <AddMax/>
                      </Route>
                      <Route path="/memberInput" render={(props) => <MemberInput {...props} /> } />
                      <Route path="/memberOutput" render={(props) => <MemberOutput {...props} /> } />
                    </Switch>
                </AnimatePresence>
            )} />
          </div>
        </Router>
  );
}

export default App;
